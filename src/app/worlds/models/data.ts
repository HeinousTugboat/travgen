export const SizeTable: [string, string, string, string | number][] = [
  ['0', '800 km', 'Asteroid, orbital complex', 'Negligible'],
  ['1', '1,600 km', '', 0.05],
  ['2', '3,200 km', 'Triton, Luna, Europa', 0.15],
  ['3', '4,800 km', 'Mercury, Ganymede', 0.25],
  ['4', '6,400 km', 'Mars', 0.35],
  ['5', '8,000 km', '', 0.45],
  ['6', '9,600 km', '', 0.7],
  ['7', '11,200 km', '', 0.9],
  ['8', '12,800 km', 'Earth', 1.0],
  ['9', '14,400 km', '', 1.25],
  ['A', '16,000 km', '', 1.4]
];

export const AtmosphereTable: [string, string, string, string, string][] = [
  ['0', 'None', 'Moons', '0.00', 'Vacc Suit'],
  ['1', 'Trace', 'Mars', '0.001 to 0.09', 'Vacc Suit'],
  ['2', 'Very Thin, Tainted', '', '0.1 to 0.42', 'Respirator, Filter'],
  ['3', 'Very Thin', '', '0.1 to 0.42', 'Respirator'],
  ['4', 'Thin, Tainted', '', '0.43 to 0.7', 'Filter'],
  ['5', 'Thin', '', '0.43 to 0.7', ''],
  ['6', 'Standard', 'Earth', '0.71 to 1.49', ''],
  ['7', 'Standard, Tainted', '', '0.71 to 1.49', 'Filter'],
  ['8', 'Dense', '', '1.5 to 2.49', ''],
  ['9', 'Dense, Tainted', '', '1.5 to 2.49', ''],
  ['A', 'Exotic', '', 'Varies', 'Air Supply'],
  ['B', 'Corrosive', 'Venus', 'Varies', 'Vacc Suit'],
  ['C', 'Insidious', '', 'Varies', 'Vacc Suit'],
  ['D', 'Dense, High', '', '2.5+', ''],
  ['E', 'Thin, Low', '', '0.5 or less', ''],
  ['F', 'Unusual', '', 'Varies', 'Varies']
];

export enum Temperature {
  FROZEN,
  COLD,
  TEMPERATE,
  HOT,
  ROASTING
}

export const TemperatureTable = {
  FROZEN: {
    roll: { max: 2 },
    type: 'Frozen',
    average: '-51 or less',
    description: 'Frozen world. No liquid water, very dry atmosphere'
  },
  COLD: {
    roll: { min: 3, max: 4 },
    type: 'Cold',
    average: '-51 to 0',
    description: 'Ice world. Little liquid water, extensive ice caps, few clouds.'
  },
  TEMPERATE: {
    roll: { min: 5, max: 9},
    type: 'Temperate',
    average: '0 to 30',
    description: 'Temperate world. Earthlike. Liquid and vaporised water are common, moderate ice caps.'
  },
  HOT: {
    roll: { min: 10, max: 11 },
    type: 'Hot',
    average: '31 to 80',
    description: 'Hot world. Small or no ice caps, little liquid water. Most water in the form of clouds.'
  },
  ROASTING: {
    roll: { min: 12 },
    type: 'Roasting',
    average: '81 or higher',
    description: 'Boiling world. No ice caps, little liquid water.'
  }
};

export const HydrographicsTable: [string, string, string][] = [
  ['0', '0% - 5%', 'Desert world'],
  ['1', '6% - 15%', 'Dry world'],
  ['2', '16% - 25%', 'A few small seas.'],
  ['3', '26% - 35%', 'Small seas and oceans.'],
  ['4', '36% - 45%', 'Wet world'],
  ['5', '46% - 55%', 'Large oceans'],
  ['6', '56% - 65%', ''],
  ['7', '66% - 75%', 'Earth-like world'],
  ['8', '76% - 85%', 'Water world'],
  ['9', '86% - 95%', 'Only a few small islands and archipelagos.'],
  ['A', '96% - 100%', 'Almost entirely water.']
];

export const PopulationTable: [string, string, string, string][] = [
  ['0', 'None', '0', ''],
  ['1', 'Few', '1+', 'A tiny farmstead or a single family'],
  ['2', 'Hundreds', '100+', 'A village'],
  ['3', 'Thousands', '1,000+', ''],
  ['4', 'Tens of Thousands', '10,000+', 'Small town'],
  ['5', 'Hundreds of thousands', '100,000+', 'Average city'],
  ['6', 'Millions', '1,000,000+', ''],
  ['7', 'Tens of millions', '10,000,000+', 'Large city'],
  ['8', 'Hundreds of millions', '100,000,000+', ''],
  ['9', 'Billions', '1,000,000,000+', 'Present day Earth'],
  ['A', 'Tens of billions', '10,000,000,000+', ''],
  ['B', 'Hundreds of billions', '100,000,000,000+', 'Incredibly crowded world'],
  ['C', 'Trillions', '1,000,000,000,000+', 'World-city']
];

export enum FactionSupport {
  OBSCURE,
  FRINGE,
  MINOR,
  NOTABLE,
  SIGNIFICANT,
  OVERWHELMING
}

export const FactionSupportTable = {
  OBSCURE: {
    roll: {min: 1, max: 3},
    strength: 'Obscure group - few have heard of them, no popular support'
  },
  FRINGE: {
    roll: {min: 4, max: 5},
    strength: 'Fringe group - few supporters'
  },
  MINOR: {
    roll: {min: 6, max: 7},
    strength: 'Minor group - some supporters'
  },
  NOTABLE: {
    roll: {min: 8, max: 9},
    strength: 'Notable group - significant support, well known'
  },
  SIGNIFICANT: {
    roll: {min: 10, max: 11},
    strength: 'Significant - nearly as powerful as the government'
  },
  OVERWHELMING: {
    roll: {min: 12, max: 12},
    strength: 'Overwhelming popular support - more powerful than the government'
  }
};

export const GovernmentTable: [string, string, string, string, string][] = [
  ['0', 'None', 'No government structure. In many cases, family bonds predominate.', 'Family, Clan, Anarchy.', 'None'],
  ['1', 'Company/corporation',
    'Ruling functions are assumed by a company managerial elite, and most citizenry are company ' +
    'employees or dependants.', 'Corporate outpost, asteroid mine, feudal domain.', 'Weapons, Drugs, Travellers'],
  ['2', 'Participating democracy', 'Ruling functions are reached by the advice and consent of the citizenry directly.',
    'Collective, tribal council, comm-linked consensus', 'Drugs'],
  ['3', 'Self-perpetuating oligarchy', 'Ruling functions are performed by a restricted minority, with ' +
    'little or no input from the mass of citizenry.', 'Plutocracy, hereditary ruling caste.', 'Technology, Weapons, Travellers'],
  ['4', 'Representative democracy', 'Ruling functions are performed by elected representatives.',
    'Republic, democracy', 'Drugs, Weapons, Psionics'],
  ['5', 'Feudal technocracy', 'Ruling fucntions are performed by specific individuals for persons who ' +
    'agree to be ruled by them. Relationships are based on the performance of technical activities which are mutually beneficial.',
    '', 'Technology, Weapons, Computers'],
  ['6', 'Captive government', 'Ruling functions are performed by an imposed leadership answerable to ' +
    'an outside group.', 'A colony or conquered area.', 'Weapons, Technology, Travellers'],
  ['7', 'Balkanisation', 'No central authority exists; rival governments compete for control. Law level ' +
    'refers to the government nearest the starport.', 'Multiple governments, civil war.', 'Varies'],
  ['8', 'Civil service bureaucracy', 'Ruling functions are performed by government agencies employing ' +
    'individuals selected for their expertise.', 'Technocracy, Communism.', 'Drugs, Weapons'],
  ['9', 'Impersonal bureaucracy', 'Ruling functions are performed by agencies which have become insulated ' +
    'from the governed citizens.', 'Entrenched castes of bureacrats, decaying empire.', 'Technology, Weapons, Drugs, Travellers, Psionics'],
  ['A', 'Charismatic dictator', 'Ruling functions are performed by agencies directed by a single leader ' +
    'who enjoys the overwhelming confidence of the citizens.', 'Revolutionary leader, messiah, emperor.', 'None'],
  ['B', 'Non-charismatic leader', 'A previous charismatic dictator has been replaced by a leader through ' +
    'normal channels.', 'Military dictatorship, hereditary kingship.', 'Weapons, Technology, Computers'],
  ['C', 'Charismatic oligarchy', 'Ruling functions are performed by a select group of members of an ' +
    'organization or class which enjoys the overwhelming confidence of the citizenry.', 'Junta, revolutionary council.', 'Weapons'],
  ['D', 'Religious dictatorship', 'Ruling functions are performed by a religious organization without ' +
    'regard to the specific individual needs of the citizenry.', 'Cult, transcendent philosophy, psionic group mind.', 'Varies'],
];

export enum Starport {
  A = 'A', B = 'B', C = 'C', D = 'D', E = 'E', X = 'X'
}

export const StarportInfo = {
  A: {
    quality: 'Excellent',
    berthingCost: '1d6 x1000',
    fuel: 'Refined',
    facilities: 'Shipyard (all), Repair',
    bases: 'Naval 8+, Scout 10+, Research 8+, TAS 4+, Imperial Consulate 6+'
  },
  B: {
    quality: 'Good',
    berthingCost: '1d6 x500',
    fuel: 'Refined',
    facilities: 'Shipyard (spacecraft), Repair',
    bases: 'Naval 8+, Scout 8+, Research 10+, TAS 6+, Imperial Consulate 8+, Pirate 12+'
  },
  C: {
    quality: 'Routine',
    berthingCost: '1d6 x100',
    fuel: 'Unrefined',
    facilities: 'Shipyard (small craft), Repair',
    bases: 'Scout 8+, Research 10+, TAS 10+, Imperial Consulate 10+, Pirate 10+'
  },
  D: {
    quality: 'Poor',
    berthingCost: '1d6 x10',
    fuel: 'Unrefined',
    facilities: 'Limited Repair',
    bases: 'Scout 7+, Pirate 12+'
  },
  E: {
    quality: 'Frontier',
    berthingCost: '0',
    fuel: 'None',
    facilities: 'None',
    bases: 'Pirate 12+'
  },
  X: {
    quality: 'No Starport',
    berthingCost: '0',
    fuel: 'None',
    facilities: 'None',
    bases: 'None'
  }
};

export enum Base {
  NAVAL, SCOUT, RESEARCH, TAS, IMPERIAL, PIRATE
}
