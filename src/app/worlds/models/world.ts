import {
  AtmosphereTable,
  Base,
  GovernmentTable,
  HydrographicsTable,
  PopulationTable,
  SizeTable,
  Starport,
  Temperature,
} from './data';

export interface Faction {
  government: number;
  strength: number;
}

export interface World {
  id: number;
  name: string;
  size: number;
  atmosphere: number;
  temperature: Temperature;
  hydrographics: number;
  population: number;
  government: number;
  factionNumber: number;
  factions: Faction[];
  law: number;
  culturalDifferences: number;
  starport: Starport;
  techLevel: number;
  uwp: string;
  base: Base[];
}

// tslint:disable:curly
export function calcStarport(x: number): Starport {
  if (x <= 2) return Starport.X;
  else if (x <= 4) return Starport.E;
  else if (x <= 6) return Starport.D;
  else if (x <= 8) return Starport.C;
  else if (x <= 10) return Starport.B;
  return Starport.A;
} // tslint:enable:curly

export function isAmber(world: World): boolean {
  return world.atmosphere >= 10 ||
    world.government === 0 ||
    world.government === 7 ||
    world.government === 10 ||
    world.law === 0 ||
    world.law >= 9;
}

export function getUWP(world: World): string {
  // debugger;
  let uwp = '';
  uwp += world.name !== undefined ? world.name.padEnd(16, ' ') : '                ';
  uwp += Math.floor(Math.random() * 100).toString().padStart(2, '0');
  uwp += Math.floor(Math.random() * 100).toString().padStart(2, '0');
  uwp += '    ';
  uwp += world.starport || '_';
  uwp += world.size !== undefined ? SizeTable[world.size][0] : '_';
  uwp += world.atmosphere !== undefined ? AtmosphereTable[world.atmosphere][0] : '_';
  uwp += world.hydrographics !== undefined ? HydrographicsTable[world.hydrographics][0] : '_';
  uwp += world.population !== undefined ? PopulationTable[world.population][0] : '_';
  uwp += world.government !== undefined ? GovernmentTable[world.government][0] : '_';
  uwp += world.law ? world.law : '_';
  uwp += '-';
  uwp += world.techLevel ? world.techLevel.toString().padEnd(2, ' ') : '_ ';
  uwp += ' ';
  uwp += isAmber(world) ? 'A' : ' ';
  return uwp.split('').map(x => x === ' ' ? '\u00A0' : x).join('');
  // return uwp;
}

/**
 * isMinEnviroTech: returns whether world is above its minimum tech level threshold for its environment
 *
 * @export
 * @param {World} world
 * @returns {boolean}
 */
export function isMinEnviroTech(world: World): boolean {
  switch (world.atmosphere) {
    case 0:
    case 1:
    case 10:
    case 15:
      if (world.techLevel < 8) { return false; }
      break;
    case 2:
    case 3:
    case 13:
    case 14:
      if (world.techLevel < 5) { return false; }
      break;
    case 4:
    case 7:
    case 9:
      if (world.techLevel < 3) { return false; }
      break;
    case 11:
      if (world.techLevel < 9) { return false; }
      break;
    case 12:
      if (world.techLevel < 10) { return false; }
      break;
  }
  return true;
}

export function d6(n = 1): number {
  let total = 0;
  for (const i = n; n; --n) {
    total += Math.floor(Math.random() * 6) + 1;
  }
  return total;
  // return Math.floor(Math.random() * 6) + 1;
}

export function d3(): number {
  return Math.floor(Math.random() * 3) + 1;
}
