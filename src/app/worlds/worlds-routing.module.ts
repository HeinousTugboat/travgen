import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WorldsComponent } from './containers/worlds/worlds.component';

const routes = [
  { path: '', component: WorldsComponent },
  { path: ':id', component: WorldsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorldsRoutingModule { }
