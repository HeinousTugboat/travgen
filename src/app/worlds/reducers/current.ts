import { Action } from '@ngrx/store';
import { WorldCurrentActions, WorldCurrentActionTypes } from '../actions/current';
import { assertNever } from '../../shared/never.util';

export type State = number;

const initialState: State = undefined;

export function reducer(state: State = initialState, action: WorldCurrentActions): State {
  switch (action.type) {
    case WorldCurrentActionTypes.SelectWorld:
      return action.id;
    case WorldCurrentActionTypes.DeleteWorld:
      return action.id === state ? undefined : state;
    default:
      // assertNever(action);
      return state;
  }
}
