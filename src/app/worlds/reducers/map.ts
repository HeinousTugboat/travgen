import { Action } from '@ngrx/store';

import { WorldCurrentActions, WorldCurrentActionTypes } from '../actions/current';
import { WorldRollActions, WorldRollActionTypes } from '../actions/map';
import { Starport, Temperature } from '../models/data';
import { calcStarport, Faction, getUWP, World } from '../models/world';

export interface State {
  [k: number]: World;
}

const initialState: State = {};
const clamp = (n: number, a: number, b: number): number => n <= a ? a : n >= b ? b : n;

export function reducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    default:
      return state;
  }
}
// tslint:disable:curly
export function worldReducer(state: State = initialState, action: WorldRollActions | WorldCurrentActions): State {
  if (action.type === WorldCurrentActionTypes.AddWorld) return state;
  const world = { ...state[action.id] } || {} as World;
  if (!world.name) world.name = 'Unnamed World';
  if (!world.uwp) world.uwp = getUWP(world);
  switch (action.type) {
    case WorldCurrentActionTypes.DeleteWorld:
      const { [action.id]: tempWorld, ...newState } = state;
      return newState;
    case WorldRollActionTypes.GenerateName:
      world.name = action.name;
      break;
    case WorldRollActionTypes.RollAtmosphere:
      if (world.size === undefined) return state;
      world.atmosphere = clamp(action.atmosphere + world.size, 0, 15);
      break;
    case WorldRollActionTypes.RollCulturalDifferences:
      world.culturalDifferences = action.culturalDifferences; break;
    case WorldRollActionTypes.RollFactionGovernment:
      if (world.factions[action.faction]) {
        world.factions[action.faction].government = action.government;
      } else {
        world.factions[action.faction] = { government: action.government, strength: null };
      }
      break;
    case WorldRollActionTypes.RollFactionNumber:
      world.factions = (new Array(action.factionNumber)).fill(({ government: null, strength: null }));
      break;
    case WorldRollActionTypes.RollFactionStrength:
      if (world.factions[action.faction]) {
        world.factions[action.faction].strength = action.factionStrength;
      } else {
        world.factions[action.faction] = { government: null, strength: action.factionStrength };
      }
      break;
    case WorldRollActionTypes.RollGovernment:
      if (world.population === undefined) break;
      world.government = clamp(action.government + world.population, 0, 13);
      break;
    case WorldRollActionTypes.RollHydrographics:
      if (world.atmosphere === undefined || world.size === undefined) return state;
      let hydrographics = action.hydrographics;
      if (world.size === 0 || world.size === 1) hydrographics = 0;
      else hydrographics += world.size;

      switch (world.atmosphere) {
        case 0:
        case 1:
        case 10:
        case 11:
        case 12:
          hydrographics -= 4;
          break;
        case 13:
          if (world.temperature === undefined) return state;
          if (world.temperature === Temperature.HOT) hydrographics -= 2;
          else if (world.temperature === Temperature.ROASTING) hydrographics -= 6;
          break;
      }
      world.hydrographics = clamp(hydrographics, 0, 10);
      break;
    case WorldRollActionTypes.RollLaw:
      if (world.government === undefined) return state;
      world.law = clamp(action.law + world.government, 0, 9);
      break;
    case WorldRollActionTypes.RollPopulation:
      world.population = action.population;
      if (world.population === 0) {
        world.government = 0;
        world.law = 0;
        world.techLevel = 0;
      }
      break;
    case WorldRollActionTypes.RollSize:
      world.size = action.size;
      break;
    case WorldRollActionTypes.RollStarport:
      world.starport = calcStarport(action.starport);
      break;
    case WorldRollActionTypes.RollTechLevel:
      if (world.size === undefined ||
        world.atmosphere === undefined ||
        world.hydrographics === undefined ||
        world.population === undefined ||
        world.government === undefined) return state;
      let techLevel = action.techLevel;
      switch (world.starport) {
        case Starport.A: techLevel += 6; break;
        case Starport.B: techLevel += 4; break;
        case Starport.C: techLevel += 2; break;
        case Starport.X: techLevel -= 4; break;
      }

      if (world.size <= 1) techLevel += 2;
      else if (world.size <= 4) techLevel += 1;

      if (world.atmosphere <= 4 || world.atmosphere >= 10) techLevel += 1;

      if (world.hydrographics === 0 || world.hydrographics === 9) techLevel += 1;
      else if (world.hydrographics === 10) techLevel += 2;

      if (world.population > 0 && world.population <= 5 || world.population === 9) techLevel += 1;
      else if (world.population === 10) techLevel += 2;
      else if (world.population === 11) techLevel += 3;
      else if (world.population === 12) techLevel += 4;

      if (world.government === 0 || world.government === 5) techLevel += 1;
      else if (world.government === 7) techLevel += 2;
      else if (world.government === 13 || world.government === 14) techLevel -= 2;

      if (techLevel < 0) techLevel = 0;
      world.techLevel = techLevel;
      break;
    case WorldRollActionTypes.RollTemperature:
      if (world.atmosphere === undefined) return state;
      let dm = 0;
      const atmo = world.atmosphere;
      if (atmo <= 1) dm = 0;
      else if (atmo <= 3) dm = -2;
      else if (atmo <= 5 || atmo === 14) dm = -1;
      else if (atmo <= 7) dm = 0;
      else if (atmo <= 9) dm = 1;
      else if (atmo === 10 || atmo === 13 || atmo === 15) dm = 2;
      else if (atmo <= 12) dm = 6;

      const result = action.temperature + dm;
      let temperature: Temperature;
      if (result <= 2) temperature = Temperature.FROZEN;
      else if (result <= 4) temperature = Temperature.COLD;
      else if (result <= 9) temperature = Temperature.TEMPERATE;
      else if (result <= 11) temperature = Temperature.HOT;
      else temperature = Temperature.ROASTING;
      world.temperature = temperature;
      break;

    default: return state;
  }
  world.uwp = getUWP(world);
  return { ...state, [action.id]: world };
}
// tslint:enable:curly
