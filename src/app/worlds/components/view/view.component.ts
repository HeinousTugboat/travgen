import { Component, EventEmitter, Input, OnInit, Output, ChangeDetectionStrategy } from '@angular/core';

import { actionsArray, WorldRollActions, WorldRollActionTypes } from '../../actions/map';
import * as data from '../../models/data';
import { isMinEnviroTech, World } from '../../models/world';

@Component({
  selector: 'app-view',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  data = data;
  actions = WorldRollActionTypes;
  @Input() world: World;
  @Input() availableActions: Map<WorldRollActionTypes, boolean>;
  @Output() click = new EventEmitter();
  constructor() { }

  ngOnInit() { }
  checkEnviroTech(world: World) { return isMinEnviroTech(world); }

}
