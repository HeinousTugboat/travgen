import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { assertNever } from '../../../shared/never.util';
import { AddWorld, DeleteWorld, SelectWorld } from '../../actions/current';
import {
  actionsMap,
  GenerateName,
  RollAtmosphere,
  RollCulturalDifferences,
  RollFactionGovernment,
  RollFactionNumber,
  RollGovernment,
  RollHydrographics,
  RollLaw,
  RollPopulation,
  RollSize,
  RollStarport,
  RollTechLevel,
  RollTemperature,
  WorldRollActionTypes,
} from '../../actions/map';
import { World } from '../../models/world';
import { State } from '../../worlds.module';

@Component({
  selector: 'app-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
  worlds$: Observable<State>;
  subscription: Subscription;
  worlds: [string, World][] = [];
  worldMap: { [key: number]: World };
  current: number;
  actions: Map<WorldRollActionTypes, boolean>;

  constructor(private store: Store<State>) {
    this.worlds$ = store.pipe(select('worlds'));
    this.subscription = this.worlds$.subscribe(state => {
      this.current = state.current;
      this.worldMap = state.map;
      this.worlds = Object.entries(state.map) as [string, World][];
      if (state.current && state.map[state.current]) {
        this.actions = actionsMap(state.map[state.current]);
      } else {
        this.actions = new Map<WorldRollActionTypes, boolean>();
      }
    });
  }
  trackWorld(index: number, item: World) {
    return item.id;
  }
  ngOnInit() { }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  add() {
    this.store.dispatch(new AddWorld);
  }

  select(world: number) {
    this.store.dispatch(new SelectWorld(world));
  }
  delete(world: number) {
    this.store.dispatch(new DeleteWorld(world));
  }
  dispatch(action: WorldRollActionTypes) {
    switch (action) {
      case WorldRollActionTypes.RollSize:
        this.store.dispatch(new RollSize(this.current)); break;
      case WorldRollActionTypes.RollPopulation:
        this.store.dispatch(new RollPopulation(this.current)); break;
      case WorldRollActionTypes.RollAtmosphere:
        this.store.dispatch(new RollAtmosphere(this.current)); break;
      case WorldRollActionTypes.RollCulturalDifferences:
        this.store.dispatch(new RollCulturalDifferences(this.current)); break;
      case WorldRollActionTypes.RollGovernment:
        this.store.dispatch(new RollGovernment(this.current)); break;
      case WorldRollActionTypes.RollHydrographics:
        this.store.dispatch(new RollHydrographics(this.current)); break;
      case WorldRollActionTypes.RollLaw:
        this.store.dispatch(new RollLaw(this.current)); break;
      case WorldRollActionTypes.RollStarport:
        this.store.dispatch(new RollStarport(this.current)); break;
      case WorldRollActionTypes.RollTechLevel:
        this.store.dispatch(new RollTechLevel(this.current)); break;
      case WorldRollActionTypes.RollTemperature:
        this.store.dispatch(new RollTemperature(this.current)); break;
      case WorldRollActionTypes.RollFactionNumber:
        this.store.dispatch(new RollFactionNumber(this.current)); break;
      case WorldRollActionTypes.GenerateName:
        this.store.dispatch(new GenerateName); break;
      case WorldRollActionTypes.RollFactionGovernment:
      case WorldRollActionTypes.RollFactionStrength:
        break;
      default:
        // assertNever(action);
        break;
    }
  }
}
