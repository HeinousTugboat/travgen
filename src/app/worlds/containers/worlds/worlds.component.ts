import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { map, tap } from 'rxjs/operators';
import { SelectWorld } from '../../actions/current';
import * as fromCurrent from '../../reducers/current';
import * as fromMap from '../../reducers/map';
import { State } from '../../worlds.module';
import { Observable } from 'rxjs/Observable';
import { World } from '../../models/world';

@Component({
  selector: 'app-worlds',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './worlds.component.html',
  styleUrls: ['./worlds.component.scss']
})
export class WorldsComponent implements OnInit, OnDestroy {
  private routeSub: Subscription;
  private worldSub: Subscription;
  private worlds$: Observable<State>;
  private worldMap: fromMap.State;
  private currentWorldRoute: number;
  private worldState: State;
  constructor(store: Store<State>, route: ActivatedRoute, private router: Router) {
    this.worlds$ = store.pipe(select('worlds'));
    this.worldSub = this.worlds$.subscribe(x => {
      this.worldState = x;
      this.worldMap = x.map;
    });

    this.routeSub = route.params.pipe(
      map(params => new SelectWorld(params.id))
    ).subscribe(store);
  }

  ngOnInit() { }
  ngOnDestroy() {
    this.routeSub.unsubscribe();
    this.worldSub.unsubscribe();
  }

}
