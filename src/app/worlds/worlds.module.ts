import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { ActionReducerMap, StoreModule } from '@ngrx/store';

import { SharedModule } from '../shared/shared.module';
import { ViewComponent } from './components/view/view.component';
import { ListComponent } from './containers/list/list.component';
import { WorldsComponent } from './containers/worlds/worlds.component';
import { CurrentEffects } from './effects/current.effects';
import * as fromCurrent from './reducers/current';
import * as fromMap from './reducers/map';
import { WorldsRoutingModule } from './worlds-routing.module';

export interface State {
  current: fromCurrent.State;
  map: fromMap.State;
}

export const reducers: ActionReducerMap<State> = {
  current: fromCurrent.reducer,
  map: fromMap.worldReducer
};

@NgModule({
  imports: [
    SharedModule,
    WorldsRoutingModule,
    StoreModule.forFeature('worlds', reducers),
    EffectsModule.forFeature([CurrentEffects])
  ],
  providers: [CurrentEffects],
  declarations: [ListComponent, ViewComponent, WorldsComponent],
  exports: [ListComponent, ViewComponent]
})
export class WorldsModule { }
