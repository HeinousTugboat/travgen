import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Action, Store, select } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AddWorld, SelectWorld, DeleteWorld } from '../actions/current';
import { WorldCurrentActionTypes } from '../actions/current';
import { tap, map, catchError, filter } from 'rxjs/operators';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';
import { GenerateName, WorldRollActionTypes } from '../actions/map';
import { Router } from '@angular/router';
import { State } from '../worlds.module';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class CurrentEffects {
  // First we generate ze name..
  @Effect() current$: Observable<GenerateName> = this.actions$.pipe(
    ofType<AddWorld>(WorldCurrentActionTypes.AddWorld),
    map(() => new GenerateName));

  // Den we pick zee planet!
  @Effect() name$: Observable<SelectWorld> = this.actions$.pipe(
    ofType<GenerateName>(WorldRollActionTypes.GenerateName),
    map(x => new SelectWorld(x.id)));

  // Check if our world actually exists..
  @Effect({ dispatch: false }) select$: Observable<SelectWorld> = this.actions$.pipe(
    ofType<SelectWorld>(WorldCurrentActionTypes.SelectWorld),
    tap(action => {
      if (action.id === undefined || this.worldState.map[action.id] === undefined) {
        this.router.navigate(['/worlds']);
      } else {
        this.router.navigate(['/worlds', action.id]);
      }
    })
  );

  // Check if our current world exists when we delete a world..
  @Effect({ dispatch: false }) deleteName$: Observable<DeleteWorld> = this.actions$.pipe(
    ofType<DeleteWorld>(WorldCurrentActionTypes.DeleteWorld),
    tap(action => {
      GenerateName.names.delete(action.id);
      if (action.id === this.worldState.current || this.worldState.current === undefined) {
        this.router.navigate(['/worlds']);
      }
    })
  );

  private worldState: State;
  private subscription: Subscription;
  constructor(private actions$: Actions, private router: Router, store: Store<State>) {
    this.subscription = store.pipe(select('worlds')).subscribe((x: State) => {
      this.worldState = x;
    });
  }
}
