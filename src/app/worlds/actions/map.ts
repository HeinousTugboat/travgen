import { Action } from '@ngrx/store';

import { Starport } from '../models/data';
import { d3, d6, Faction, World } from '../models/world';

export enum WorldRollActionTypes {
  RollSize = '[World] Roll Size',
  RollAtmosphere = '[World] Roll Atmosphere',
  RollTemperature = '[World] Roll Temperature',
  RollHydrographics = '[World] Roll Hydrography',
  RollPopulation = '[World] Roll Population',
  RollGovernment = '[World] Roll Government',
  RollFactionNumber = '[World] Roll Number of Factions',
  RollFactionStrength = '[World] Roll Faction Strength',
  RollFactionGovernment = '[World] Roll Faction Government',
  RollLaw = '[World] Roll Law Level',
  RollCulturalDifferences = '[World] Roll Cultural Differences',
  RollStarport = '[World] Roll Starport',
  RollTechLevel = '[World] Roll Tech Level',
  GenerateName = '[World] Generate Name'
}

export interface RollAction extends Action {
  id: number;
}

export class RollSize implements RollAction {
  readonly type = WorldRollActionTypes.RollSize;
  size = d6(2) - 2;
  constructor(public id: number) {
  }
  static canUse(world: World): boolean {
    return true;
  }
}

export class RollAtmosphere implements RollAction {
  readonly type = WorldRollActionTypes.RollAtmosphere;
  atmosphere = d6(2) - 7;
  constructor(public id: number) { }
  static canUse(world: World): boolean {
    return world.size !== undefined;
  }
}

export class RollTemperature implements RollAction {
  readonly type = WorldRollActionTypes.RollTemperature;
  temperature = d6(2);
  constructor(public id: number) { }
  static canUse(world: World): boolean {
    return world.atmosphere !== undefined;
  }
}

export class RollHydrographics implements RollAction {
  readonly type = WorldRollActionTypes.RollHydrographics;
  hydrographics = d6(2) - 7;
  constructor(public id: number) { }
  static canUse(world: World): boolean {
    return world.size !== undefined &&
      world.atmosphere !== undefined &&
      world.temperature !== undefined;
  }
}

export class RollPopulation implements RollAction {
  readonly type = WorldRollActionTypes.RollPopulation;
  population = d6(2) - 2;
  constructor(public id: number) { }
  static canUse(world: World): boolean {
    return true;
  }
}

export class RollGovernment implements RollAction {
  readonly type = WorldRollActionTypes.RollGovernment;
  government = d6(2) - 7;
  constructor(public id: number) { }
  static canUse(world: World): boolean {
    return world.population !== undefined && world.population > 0;
  }
}

export class RollFactionNumber implements RollAction {
  readonly type = WorldRollActionTypes.RollFactionNumber;
  factionNumber = d3();
  constructor(public id: number) { }
  static canUse(world: World): boolean {
    return true;
  }
}

export class RollFactionStrength implements RollAction {
  readonly type = WorldRollActionTypes.RollFactionStrength;
  factionStrength = d6(2);
  constructor(public id: number, public faction: number) { }
  static canUse(world: World): boolean {
    return world.factionNumber !== undefined && world.factionNumber > 0;
  }
}

export class RollFactionGovernment implements RollAction {
  readonly type = WorldRollActionTypes.RollFactionGovernment;
  government = d6(2) - 7;
  constructor(public id: number, public faction: number) { }
  static canUse(world: World): boolean {
    return world.factionNumber !== undefined && world.factionNumber > 0;
  }
}

export class RollLaw implements RollAction {
  readonly type = WorldRollActionTypes.RollLaw;
  law = d6(2) - 7;
  constructor(public id: number) { }
  static canUse(world: World): boolean {
    return world.population !== undefined && world.population > 0;
  }
}

export class RollCulturalDifferences implements RollAction {
  readonly type = WorldRollActionTypes.RollCulturalDifferences;
  culturalDifferences = d6(); // FIXME: No idea what this should be..
  constructor(public id: number) { }
  static canUse(world: World): boolean {
    return true;
  }
}

export class RollStarport implements RollAction {
  readonly type = WorldRollActionTypes.RollStarport;
  starport = d6(2);
  constructor(public id: number) { }
  static canUse(world: World): boolean {
    return true;
  }
}

export class RollTechLevel implements RollAction {
  readonly type = WorldRollActionTypes.RollTechLevel;
  techLevel = d6();
  constructor(public id: number) { }
  static canUse(world: World): boolean {
    return world.starport !== undefined &&
      world.size !== undefined &&
      world.atmosphere !== undefined &&
      world.hydrographics !== undefined &&
      world.population !== undefined &&
      world.government !== undefined;
  }
}

export class GenerateName implements RollAction {
  static names: Map<number, string> = new Map;
  static ids = 1000;
  readonly type = WorldRollActionTypes.GenerateName;
  name: string;
  id: number = GenerateName.ids++;
  constructor() {
    let name = 'Test';
    const names = Array.from(GenerateName.names.values());
    do {
      name = 'Blank-' + Math.floor(Math.random() * 1000).toString().padStart(4, '0');
    } while (names.includes(name));
    this.name = name;
    GenerateName.names.set(this.id, name);
  }
  static canUse(world: World): boolean {
    return true;
  }
}

export function actionsMap(world: World): Map<WorldRollActionTypes, boolean> {
  const actions = new Map<WorldRollActionTypes, boolean>();
  actions.set(WorldRollActionTypes.RollSize, RollSize.canUse(world));
  actions.set(WorldRollActionTypes.RollAtmosphere, RollAtmosphere.canUse(world));
  actions.set(WorldRollActionTypes.RollTemperature, RollTemperature.canUse(world));
  actions.set(WorldRollActionTypes.RollHydrographics, RollHydrographics.canUse(world));
  actions.set(WorldRollActionTypes.RollPopulation, RollPopulation.canUse(world));
  actions.set(WorldRollActionTypes.RollGovernment, RollGovernment.canUse(world));
  actions.set(WorldRollActionTypes.RollFactionGovernment, RollFactionGovernment.canUse(world));
  actions.set(WorldRollActionTypes.RollFactionNumber, RollFactionNumber.canUse(world));
  actions.set(WorldRollActionTypes.RollFactionStrength, RollFactionStrength.canUse(world));
  actions.set(WorldRollActionTypes.RollLaw, RollLaw.canUse(world));
  actions.set(WorldRollActionTypes.RollCulturalDifferences, RollCulturalDifferences.canUse(world));
  actions.set(WorldRollActionTypes.RollStarport, RollStarport.canUse(world));
  actions.set(WorldRollActionTypes.RollTechLevel, RollTechLevel.canUse(world));
  actions.set(WorldRollActionTypes.GenerateName, GenerateName.canUse(world));

  return actions;
}

// tslint:disable:curly
export function actionsArray(world: World): WorldRollActionTypes[] {
  const actions: WorldRollActionTypes[] = [];
  if (RollSize.canUse(world)) actions.push(WorldRollActionTypes.RollSize);
  if (RollAtmosphere.canUse(world)) actions.push(WorldRollActionTypes.RollAtmosphere);
  if (RollTemperature.canUse(world)) actions.push(WorldRollActionTypes.RollTemperature);
  if (RollHydrographics.canUse(world)) actions.push(WorldRollActionTypes.RollHydrographics);
  if (RollPopulation.canUse(world)) actions.push(WorldRollActionTypes.RollPopulation);
  if (RollGovernment.canUse(world)) actions.push(WorldRollActionTypes.RollGovernment);
  if (RollFactionGovernment.canUse(world)) actions.push(WorldRollActionTypes.RollFactionGovernment);
  if (RollFactionNumber.canUse(world)) actions.push(WorldRollActionTypes.RollFactionNumber);
  if (RollFactionStrength.canUse(world)) actions.push(WorldRollActionTypes.RollFactionStrength);
  if (RollLaw.canUse(world)) actions.push(WorldRollActionTypes.RollLaw);
  if (RollCulturalDifferences.canUse(world)) actions.push(WorldRollActionTypes.RollCulturalDifferences);
  if (RollStarport.canUse(world)) actions.push(WorldRollActionTypes.RollStarport);
  if (RollTechLevel.canUse(world)) actions.push(WorldRollActionTypes.RollTechLevel);

  return actions;
} // tslint:enable:curly

export type WorldRollActions = RollSize |
  RollAtmosphere |
  RollTemperature |
  RollHydrographics |
  RollPopulation |
  RollGovernment |
  RollFactionNumber |
  RollFactionStrength |
  RollFactionGovernment |
  RollLaw |
  RollCulturalDifferences |
  RollStarport |
  RollTechLevel |
  GenerateName;
