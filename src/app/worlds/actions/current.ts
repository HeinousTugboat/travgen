import { Action } from '@ngrx/store';

import { World } from '../models/world';

export enum WorldCurrentActionTypes {
  AddWorld = '[World] Add World',
  DeleteWorld = '[World] Delete World',
  SelectWorld = '[World] Select World'

}

export class AddWorld implements Action {
  readonly type = WorldCurrentActionTypes.AddWorld;
}

export class DeleteWorld implements Action {
  readonly type = WorldCurrentActionTypes.DeleteWorld;
  constructor (public id: number) { }
}

export class SelectWorld implements Action {
  readonly type = WorldCurrentActionTypes.SelectWorld;
  constructor (public id: number) { }
}

export type WorldCurrentActions = AddWorld | DeleteWorld | SelectWorld;
