import { Action } from '@ngrx/store';

export enum TickActionTypes {
  Tick = '[Tick]'
}

export class Tick implements Action {
  readonly type = TickActionTypes.Tick;
}

export type TickActions = Tick;
