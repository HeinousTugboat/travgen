import { TickActions, TickActionTypes } from '../actions/tick';

export interface State {
  now: number;
  dt: number;
  tick: number;
}

const initialState: State = {
  now: Date.now(),
  dt: 0,
  tick: 0
};

export function reducer(state: State = initialState, action: TickActions): State {
  switch (action.type) {
    case TickActionTypes.Tick:
      const then = state.now;
      const now = Date.now();
      return {
        now, dt: now - then,
        tick: state.tick + 1
      };
  }
}
