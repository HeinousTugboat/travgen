import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './core/containers/home/home.component';
import { WorldsComponent } from './worlds/containers/worlds/worlds.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'worlds', loadChildren: 'app/worlds/worlds.module#WorldsModule' },
  // {path: 'worlds/:id', component: WorldsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
